//
//  UIViewController.swift
//  TheMeal
//
//  Created by Denny Purwana on 12/14/20.
//  Copyright © 2020 Denny Purwana. All rights reserved.
//

import Foundation
import UIKit

var vSpinner : UIView?

extension UIViewController  {
    
    
    
    func showDialog(message: String, title: String = "", handler: ((UIAlertAction) -> Void)?) {
        
        let msg = (message.isEmpty) ? "" : message
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: handler)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showDialogConfirmation(message: String, title: String, handler: ((UIAlertAction) -> Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let CancelAction = UIAlertAction(title: "BATAL", style: .default, handler: nil)
        alertController.addAction(CancelAction)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: handler)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showDetailMeals(idMeals: String) {
         
         let storyBoard: UIStoryboard = UIStoryboard(name: "Meals", bundle: nil)
         let vc = storyBoard.instantiateViewController(withIdentifier: "detailsMeals") as! MealsDetailsViewController
         vc.idMeals = idMeals
         vc.modalPresentationStyle = .fullScreen
         vc.hidesBottomBarWhenPushed = true
         vc.modalPresentationStyle = .fullScreen
         let transition = CATransition()
         transition.duration = 0.5
         transition.type = CATransitionType.push
         transition.subtype = CATransitionSubtype.fromRight
         transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
         view.window!.layer.add(transition, forKey: kCATransition)
         present(vc, animated: false, completion: nil)
         
     }
    
}
