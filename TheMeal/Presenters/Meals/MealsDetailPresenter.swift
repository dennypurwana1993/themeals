//
//  MealsDetailPresenter.swift
//  TheMeal
//
//  Created by Denny Purwana on 12/14/20.
//  Copyright © 2020 Denny Purwana. All rights reserved.
//

import Foundation
import UIKit

class MealsDetailPresenter: NSObject {
    
    private let service: MealsDetailService
    weak private var view: MealsDetailsView?
    var ingredients = [MealsIngredients]()
    init(service: MealsDetailService) {
        self.service = service
    }
    
    func attachView(view: MealsDetailsView) {
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func getDetailMeals(idMeal: String) {
        
        self.view?.startLoading()
        self.service.getDetailMeals(idMeal: idMeal,
                                    successCallback: {(data) in
                                        self.view?.stopLoading()
                                        self.setIngredients(meals: data!)
                                        self.view?.showMealsDetail(data: data)
        },
                                    errorCallback: { (errorMessage) in
                                        self.view?.stopLoading()
        })
    }
    
    func setIngredients(meals : Meals){
       
        let arrayStrIng = [meals.strIngredient1,meals.strIngredient2,meals.strIngredient3,meals.strIngredient4,meals.strIngredient5,meals.strIngredient6,meals.strIngredient7,meals.strIngredient8,meals.strIngredient9,meals.strIngredient10,meals.strIngredient11,meals.strIngredient12,meals.strIngredient13,meals.strIngredient14,meals.strIngredient15,meals.strIngredient16,meals.strIngredient17,meals.strIngredient18,meals.strIngredient19,meals.strIngredient20]
        
        let arrayStrMsm = [meals.strMeasure1,meals.strMeasure2,meals.strMeasure3,meals.strMeasure4,meals.strMeasure5,meals.strMeasure6,meals.strMeasure7,meals.strMeasure8,meals.strMeasure9,meals.strMeasure10,meals.strMeasure11,meals.strMeasure12,meals.strMeasure13,meals.strMeasure14,meals.strMeasure15,meals.strMeasure16,meals.strMeasure17,meals.strMeasure18,meals.strMeasure19,meals.strMeasure20]
        
        for index in (0...arrayStrIng.count-1) {
            let ingredient = arrayStrIng[index]
            let measurement = arrayStrMsm[index]
            if(ingredient != "" && measurement != ""){
                let mealsIngredient = MealsIngredients.init(ingredient: ingredient ?? "", measurement: measurement ?? "")
                ingredients.append(mealsIngredient)
            }
        }
        
        self.view?.showIngredients(ingredients: self.ingredients)
    }
    
}
