//
//  ListMealsPresenter.swift
//  TheMeal
//
//  Created by Denny Purwana on 12/14/20.
//  Copyright © 2020 Denny Purwana. All rights reserved.
//

import Foundation
import UIKit

class ListMealsPresenter: NSObject {
    
    private let service: ListMealsService
    weak private var view: ListMealsView?
    
    init(service: ListMealsService) {
        self.service = service
    }
    
    func attachView(view: ListMealsView) {
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func getMeals() {
        
        self.view?.startLoading()
        self.service.getMeals( successCallback: {(datas) in
            self.view?.stopLoading()
            print(datas ?? "")
            self.view?.showList(datas: datas)
        },
                               errorCallback: { (errorMessage) in
                                self.view?.stopLoading()
        })
    }
    
}
