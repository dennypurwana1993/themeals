//
//  Constants.swift
//  TheMeal
//
//  Created by Denny Purwana on 12/13/20.
//  Copyright © 2020 Denny Purwana. All rights reserved.
//

import Foundation

import UIKit

enum API {
  
    static let BaseUrl : String = "https://www.themealdb.com/api/"

}

