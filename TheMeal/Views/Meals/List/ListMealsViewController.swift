//
//  ListMealsViewController.swift
//  TheMeal
//
//  Created by Denny Purwana on 12/14/20.
//  Copyright © 2020 Denny Purwana. All rights reserved.
//

import UIKit
import SkeletonView
class ListMealsViewController: UIViewController
,UICollectionViewDataSource , SkeletonCollectionViewDataSource,
    SkeletonCollectionViewDelegate,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout,
    ListMealsView{
    
    
    @IBOutlet weak var mealsCollectionView: UICollectionView!
    var presenter = ListMealsPresenter(service: ListMealsService())
    var dataArray = [Meals]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        presenter.attachView(view: self)
        presenter.getMeals()
    }
    
    func showList(datas: [Meals]?) {
        dataArray.append(contentsOf: datas ?? [])
        self.mealsCollectionView.reloadData()
    }
    
    func startLoading() {
    }
    
    func stopLoading() {
    }
    
    func failed(code: String) {
    }
    
   
    func setupCollectionView(){
        mealsCollectionView.isSkeletonable = true
        mealsCollectionView.backgroundColor = .clear
        registerNib(collectionView: mealsCollectionView, nibName:"MealsCollectionViewCell", identifierName:"mealsCell")
    }
    
    @objc func back(){
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        self.dismiss(animated: false, completion: nil)
    }
    
    func registerNib(collectionView:UICollectionView, nibName:String, identifierName:String){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: nibName, bundle: nil), forCellWithReuseIdentifier: identifierName)
    }
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "mealsCell"
    }
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         print(dataArray.count)
        return dataArray.count
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mealsCell", for: indexPath) as! MealsCollectionViewCell
        let meal = dataArray[indexPath.row]
        cell.configure(with: meal)
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let meal = dataArray[indexPath.row]
        showDetailMeals(idMeals: meal.idMeal ?? "")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.bounds.width
        return CGSize(width: width, height: (collectionView.bounds.size.height/3) + 60 )
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}
