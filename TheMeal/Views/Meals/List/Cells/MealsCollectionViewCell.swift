//
//  MealsCollectionViewCell.swift
//  TheMeal
//
//  Created by Denny Purwana on 12/14/20.
//  Copyright © 2020 Denny Purwana. All rights reserved.
//

import UIKit
import Kingfisher
class MealsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mealsThumb: UIImageView!
    @IBOutlet weak var mealsName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func configure(with meals: Meals) {
        mealsThumb.kf.setImage(with: URL(string: meals.strMealThumb ?? ""), placeholder: UIImage(named: "goodfood"))
        mealsName.text = meals.strMeal
    }
    
    
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var frame  = layoutAttributes.frame
//        frame.size.height = 200
        layoutAttributes.frame = frame
        return layoutAttributes
    }
    
}
