//
//  ListMealsView.swift
//  TheMeal
//
//  Created by Denny Purwana on 12/14/20.
//  Copyright © 2020 Denny Purwana. All rights reserved.
//

import Foundation

protocol ListMealsView : NSObjectProtocol {
  
    func showList(datas: [Meals]?)
    func startLoading()
    func stopLoading()
    func failed(code: String)
    
}
