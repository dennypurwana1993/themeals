//
//  IngredientsCollectionViewCell.swift
//  TheMeal
//
//  Created by Denny Purwana on 12/14/20.
//  Copyright © 2020 Denny Purwana. All rights reserved.
//

import UIKit

class IngredientsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var measurementLbl: UILabel!
    @IBOutlet weak var ingredientLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    public func configure(with ingredient: MealsIngredients) {
        mainView.layer.cornerRadius = 10
        mainView.layer.masksToBounds = true
        measurementLbl.text = ingredient.measurement
        ingredientLbl.text = ingredient.ingredient
    }
    
    
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var frame  = layoutAttributes.frame
        //        frame.size.height = 200
        layoutAttributes.frame = frame
        return layoutAttributes
    }
    
    
}
