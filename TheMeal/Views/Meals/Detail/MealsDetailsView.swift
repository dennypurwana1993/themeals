//
//  MealsDetailsView.swift
//  TheMeal
//
//  Created by Denny Purwana on 12/14/20.
//  Copyright © 2020 Denny Purwana. All rights reserved.
//

import Foundation

protocol MealsDetailsView : NSObjectProtocol {
  
    func showMealsDetail(data: Meals?)
    func showIngredients(ingredients: [MealsIngredients]?)
    func startLoading()
    func stopLoading()
    func failed(code: String)
    
}
