//
//  MealsDetailsViewController.swift
//  TheMeal
//
//  Created by Denny Purwana on 12/14/20.
//  Copyright © 2020 Denny Purwana. All rights reserved.
//

import UIKit
import AVKit
import YoutubePlayer_in_WKWebView

class MealsDetailsViewController: UIViewController 
,UICollectionViewDataSource ,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout, MealsDetailsView{
    
  
    @IBOutlet weak var youtubeView: WKYTPlayerView!
    @IBOutlet weak var iconBack: UIImageView!
    @IBOutlet weak var mealsAreaView: UIView!
    @IBOutlet weak var mealsArea: UILabel!
    @IBOutlet weak var constraintHeightIngredient: NSLayoutConstraint!
    @IBOutlet weak var ingredientsCollectionView: UICollectionView!
    @IBOutlet weak var mealsInstructionView: UIView!
    @IBOutlet weak var mealsInstructions: UILabel!
    @IBOutlet weak var mealsTag: UILabel!
    @IBOutlet weak var mealsCategory: UILabel!
    @IBOutlet weak var mealsName: UILabel!
    @IBOutlet weak var mealsThumb: UIImageView!
    @IBOutlet weak var tagView: UIView!
    var avPlayer: AVPlayer!
    var presenter = MealsDetailPresenter(service: MealsDetailService())
    var idMeals = ""
    var sourceLink = ""
    var dataArray = [MealsIngredients]()
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        presenter.getDetailMeals(idMeal: idMeals)
        setupViews()
    }
    
    func setupViews(){
        let gestureBack = UITapGestureRecognizer(target: self, action:  #selector(self.back))
        let gestureOpenSource = UITapGestureRecognizer(target: self, action:  #selector(self.openSource))
        iconBack.isUserInteractionEnabled = true
        iconBack.addGestureRecognizer(gestureBack)
        mealsThumb.isUserInteractionEnabled = true
        mealsThumb.addGestureRecognizer(gestureOpenSource)
        tagView.layer.cornerRadius = 10
        tagView.layer.masksToBounds = true
        mealsAreaView.layer.cornerRadius = 10
        mealsAreaView.layer.masksToBounds = true
        mealsInstructionView.layer.cornerRadius = 10
        mealsInstructionView.layer.masksToBounds = true
        setupCollectionView()
    }
    
    func showMealsDetail(data: Meals?) {
        mealsThumb.kf.setImage(with: URL(string: data?.strMealThumb ?? ""), placeholder: UIImage(named: "goodfood"))
        mealsName.text = data?.strMeal
        mealsCategory.text = data?.strCategory
        mealsInstructions.text = data?.strInstructions
        self.sourceLink = data?.strSource ?? ""
        if(data?.strTags != ""){
            tagView.isHidden = false
            mealsTag.text = data?.strTags
        }
        if(data?.strArea != ""){
            mealsAreaView.isHidden = false
            mealsArea.text = data?.strArea
        }
        youtubeView.load(withVideoId: "itsFEc8W468")
       
    }
    
    @objc func openSource(){
        if let url = URL(string: self.sourceLink) {
                  UIApplication.shared.open(url)
              }
          }
    
    func showIngredients(ingredients: [MealsIngredients]?) {
        print(ingredients?.count ?? "")
        dataArray.append(contentsOf: ingredients ?? [])
        self.ingredientsCollectionView.reloadData()
        let height = self.ingredientsCollectionView.collectionViewLayout.collectionViewContentSize.height
        if(ingredients?.count ?? 0 > 10){
            self.constraintHeightIngredient.constant = (height/2) + 100
        } else {
            self.constraintHeightIngredient.constant = height
        }
        
        self.ingredientsCollectionView.layoutIfNeeded()
    }
    
   
    func startLoading() {
    }
    
    func stopLoading() {
    }
    
    func failed(code: String) {
    }
    
    
    func setupCollectionView(){
        
        registerNib(collectionView: ingredientsCollectionView, nibName:"IngredientsCollectionViewCell", identifierName:"ingredientCell")
    }
    
    @objc func back(){
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        self.dismiss(animated: false, completion: nil)
    }
    
    func registerNib(collectionView:UICollectionView, nibName:String, identifierName:String){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: nibName, bundle: nil), forCellWithReuseIdentifier: identifierName)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return dataArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ingredientCell", for: indexPath) as! IngredientsCollectionViewCell
        let ingredient = dataArray[indexPath.row]
        cell.configure(with: ingredient)
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.bounds.width / 6 + 50
        return CGSize(width: width, height: 120 )
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
}
