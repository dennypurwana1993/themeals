//
//  MealsResponse.swift
//  TheMeal
//
//  Created by Denny Purwana on 12/14/20.
//  Copyright © 2020 Denny Purwana. All rights reserved.
//

import Foundation

class MealsResponse: Codable {
    let meals: [Meals]?
}

//
//class MealsData: Codable {
//    let meals: [Meals]?
//}
