//
//  MealsIngredients.swift
//  TheMeal
//
//  Created by Denny Purwana on 12/14/20.
//  Copyright © 2020 Denny Purwana. All rights reserved.
//

import Foundation

class MealsIngredients {
    
    var ingredient : String?
    var measurement: String?
    
    
    init (ingredient: String, measurement: String) {
        self.ingredient = ingredient
        self.measurement = measurement
    }
    
    init() {
    }
    
}


