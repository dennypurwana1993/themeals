//
//  MealsDetailService.swift
//  TheMeal
//
//  Created by Denny Purwana on 12/14/20.
//  Copyright © 2020 Denny Purwana. All rights reserved.
//

import Foundation

class MealsDetailService: NSObject {
    
    func getDetailMeals(idMeal: String, successCallback: ((_ result: Meals?) -> Void)?,
                        errorCallback: ((_ code: String) -> Void)?)
    {
        
        let parameters: [String: Any] = [
            "i": idMeal
        ]
        print(parameters)
        providerServices.request(CommonApiService.getRequest(url: "json/v1/1/lookup.php", params: parameters , isAuth: false)) { (result) in
            switch result {
            case .success(let response):
                do {
                    //print(response)
                    let decoder = JSONDecoder()
                    if ( response.statusCode == 200){
    
                        let mealsResponse = try
                            decoder.decode(MealsResponse?.self, from: response.data)
                        
                        let data = mealsResponse?.meals ?? []
                        successCallback?(data[0])
                        
                    } else {
                        let errorResponse = try decoder.decode(ErrorResponse.self, from:response.data)
                        errorCallback?(errorResponse.code ?? "")
                        
                    }
                } catch let parsingError {
                    errorCallback?("500")
                }
            case .failure(let error):
                errorCallback?("641")
            }
        }
    }
    
}
