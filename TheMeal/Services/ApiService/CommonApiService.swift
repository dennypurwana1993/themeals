//
//  CommonApiService.swift
//  TheMeal
//
//  Created by Denny Purwana on 12/14/20.
//  Copyright © 2020 Denny Purwana. All rights reserved.
//

import Foundation
import Moya
import UIKit

let providerServices = MoyaProvider<CommonApiService>()
let userDefaults = UserDefaults.standard

enum CommonApiService {

    case getBodyRequest (url:String, params: [String : Any], isAuth: Bool)
    case postBodyRequest (url:String, params: [String : Any], isAuth: Bool)
    case postRequest    (url:String, params: [String : Any], isAuth: Bool)
    case putRequest     (url:String, params: [String : Any], isAuth: Bool)
    case getRequest     (url:String, params: [String : Any], isAuth: Bool)
    case deleteRequest  (url:String, params: [String : Any], isAuth: Bool)
    case uploadRequest  (url:String, multipartBody :[Moya.MultipartFormData]?, params: [String : Any], isAuth: Bool)
  
}

extension CommonApiService :TargetType{
    
    var baseURL: URL {
        return URL(string: API.BaseUrl)!
    }
    
    var path: String {
        
        switch self {
            
        case .postRequest(let url, _, _):
            return url
            
        case .postBodyRequest(let url, _, _):
            return url
            
        case .putRequest(let url, _, _):
            return url
            
        case .getRequest(let url, _, _):
             return url
            
        case .getBodyRequest(let url, _, _):
             return url
        
        case .deleteRequest(let url, _, _):
            return url
            
        case .uploadRequest(let url,_, _, _):
            return url
            
        }
    }
    
    var method: Moya.Method {
        switch self {
         
        case .deleteRequest:
            return .delete
            
        case .putRequest:
            return .put
            
        case .getRequest:
            return .get
            
        case .getBodyRequest:
             return .get
            
        default:
            return .post
            
        }
    }

    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
            
        case .postRequest(_, let params, _):
        return .requestParameters(
            parameters: params,
                encoding: URLEncoding.default
            )
            
        case .postBodyRequest(_, let params, _):
            return .requestParameters(
                parameters: params,
                encoding: JSONEncoding.default
            )
            
        case .putRequest(_, let params, _):
            return .requestParameters(
                parameters: params,
                encoding: URLEncoding.default
                
            )
            
        case .getRequest(_, let params, _):
            return .requestParameters(
                parameters: params,
                encoding: URLEncoding.default
            )
            
        case .getBodyRequest(_, let params, _):
                return .requestParameters(
                parameters: params,
                encoding: JSONEncoding.default
            )
            
        case .deleteRequest(_, let params, _):
            return .requestParameters(
                parameters: params,
                encoding: URLEncoding.default
            )
            
        case .uploadRequest(_, let multipartBody, _, _) :
            return .uploadMultipart(multipartBody!)

            
        }
        
    }
    
    var headers: [String : String]? {
        switch self {
       
        case .postRequest(_, _, let isAuth):
            if (isAuth) {
                return ["Authorization": "Bearer \(userDefaults.string(forKey: "access_token") ?? "")"]
            } else {
                return nil
            }
            
         case .postBodyRequest(_, _, let isAuth):
            if (isAuth) {
                return ["Authorization": "Bearer \(userDefaults.string(forKey: "access_token") ?? "")"]
            } else {
                return nil
            }
            
        case .getRequest(_, _, let isAuth):
            if (isAuth) {
                return ["Authorization": "Bearer \(userDefaults.string(forKey: "access_token") ?? "")"]
            } else {
                return nil
            }
            
        case .getBodyRequest(_, _, let isAuth):
                if (isAuth) {
                    return ["Authorization": "Bearer \(userDefaults.string(forKey: "access_token") ?? "")"]
                } else {
                           return nil
        }
            
        case .putRequest(_, _, let isAuth):
            if (isAuth) {
                return ["Authorization": "Bearer \(userDefaults.string(forKey: "access_token") ?? "")"]
            } else {
                return nil
            }
      
        case .deleteRequest(_, _, let isAuth):
            if (isAuth) {
                return ["Authorization": "Bearer \(userDefaults.string(forKey: "access_token") ?? "")"]
            } else {
                return nil
            }
            
        case .uploadRequest(_,_, _, let isAuth):
            if (isAuth) {
                return ["Authorization": "Bearer \(userDefaults.string(forKey: "access_token") ?? "")"]
            } else {
                return nil
            }
            
        }
       
    }
    
}
