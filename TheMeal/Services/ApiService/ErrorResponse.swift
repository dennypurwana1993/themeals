//
//  ErrorResponse.swift
//  TheMeal
//
//  Created by Denny Purwana on 12/14/20.
//  Copyright © 2020 Denny Purwana. All rights reserved.
//

import Foundation
struct ErrorResponse : Codable {
    
    let code: String?
    
}
